package com.showcase.doodle;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.MediaStore;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DoodlePresenter {

    private Context mContext;

    private CompositeDisposable mCompositeDisposable;

    private View mView;

    @Inject
    public DoodlePresenter(Context context) {
        mContext = context;
        mCompositeDisposable = new CompositeDisposable();
    }

    void startPresenting(View view) {
        mView = view;
    }

    void stopPresenting() {
        mCompositeDisposable.clear();
    }

    void saveDoodleToGallery(Bitmap bitmap) {
        mCompositeDisposable.add(Completable.create(emitter -> {
            try {
                String fileName = String.valueOf(System.currentTimeMillis());
                MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap, fileName, fileName);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mView::displaySaveDoodleToGallerySuccess, this::onSaveDoodleToGalleryFailed));
    }

    void setDoodleAsWallpaper(Bitmap bitmap) {
        mCompositeDisposable.add(Completable.create(emitter -> {
            WallpaperManager.getInstance(mContext).setBitmap(bitmap);
            emitter.onComplete();
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(mView::displaySetDoodleAsWallpaperSuccess, this::onSetDoodleAsWallpaperFailed));
    }

    private void onSetDoodleAsWallpaperFailed(Throwable throwable) {
        mView.displaySetDoodleAsWallpaperFailed();
    }

    private void onSaveDoodleToGalleryFailed(Throwable throwable) {
        mView.displaySaveDoodleToGalleryFailed();
    }

    interface View {
        void displaySaveDoodleToGallerySuccess();
        void displaySaveDoodleToGalleryFailed();

        void displaySetDoodleAsWallpaperSuccess();
        void displaySetDoodleAsWallpaperFailed();
    }

}
