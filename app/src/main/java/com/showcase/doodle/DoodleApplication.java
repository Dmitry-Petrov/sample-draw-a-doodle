package com.showcase.doodle;

import android.app.Application;
import android.support.annotation.NonNull;

import com.showcase.doodle.di.components.ApplicationComponent;
import com.showcase.doodle.di.components.DaggerApplicationComponent;
import com.showcase.doodle.di.modules.ApplicationModule;

public class DoodleApplication extends Application {

    private static ApplicationComponent sComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sComponent = createComponent();
        sComponent.inject(this);
    }

    protected ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    @NonNull
    public static ApplicationComponent getComponent() {
        return sComponent;
    }
}
