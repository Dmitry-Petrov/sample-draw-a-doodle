package com.showcase.doodle.di.components;

import com.showcase.doodle.DoodleFragment;
import com.showcase.doodle.di.qualifiers.PerFragment;

import dagger.Subcomponent;

@Subcomponent
@PerFragment
public interface FragmentComponent {

    void inject(DoodleFragment fragment);
}
