package com.showcase.doodle.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Context mAppContext;

    public ApplicationModule(Context context) {
        mAppContext = context.getApplicationContext();
    }

    @Singleton
    @Provides
    Context providesApplicationContext() {
        return mAppContext;
    }

}
