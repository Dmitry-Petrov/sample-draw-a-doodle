package com.showcase.doodle.di.components;

import com.showcase.doodle.DoodleActivity;
import com.showcase.doodle.di.modules.ActivityModule;
import com.showcase.doodle.di.qualifiers.PerActivity;

import dagger.Subcomponent;

@Subcomponent(modules = {ActivityModule.class})
@PerActivity
public interface ActivityComponent {

    void inject(DoodleActivity activity);

    FragmentComponent plusFragmentComponent();

}
