package com.showcase.doodle.di.modules;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.showcase.doodle.di.qualifiers.PerActivity;
import com.showcase.doodle.utils.ColorPickerHelper;
import com.showcase.doodle.utils.ViewHelper;

import dagger.Module;
import dagger.Provides;

/**
 * Module providing Activity context per Activity.
 */
@Module
public class ActivityModule {

    private final Activity mActivity;

    public ActivityModule(@NonNull final Activity activity) {
        mActivity = activity;
    }

    @PerActivity
    @Provides
    Activity provideActivity() {
        return mActivity;
    }

    @Provides
    ViewHelper providesViewHelper(@PerActivity Activity activity) {
        return new ViewHelper(activity);
    }

    @Provides
    ColorPickerHelper providesColorPickerHelper(@PerActivity Activity activity) {
        return new ColorPickerHelper(activity);
    }

}
