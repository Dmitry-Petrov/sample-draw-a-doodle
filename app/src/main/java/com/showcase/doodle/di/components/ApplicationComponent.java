package com.showcase.doodle.di.components;


import com.showcase.doodle.DoodleApplication;
import com.showcase.doodle.di.modules.ActivityModule;
import com.showcase.doodle.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {ApplicationModule.class,})
@Singleton
public interface ApplicationComponent {

    void inject(DoodleApplication application);

    ActivityComponent plus(ActivityModule activityModule);
}
