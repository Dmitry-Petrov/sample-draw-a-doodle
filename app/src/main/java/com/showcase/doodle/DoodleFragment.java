package com.showcase.doodle;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.showcase.doodle.utils.ColorPickerHelper;
import com.showcase.doodle.utils.ViewHelper;
import com.showcase.doodle.view.ChooseBrushSizeDialogFragment;
import com.showcase.doodle.view.DrawingView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoodleFragment extends Fragment implements DoodlePresenter.View {

    private static final int SELECT_PHOTO_RESULT = 1;
    private static final int REQUEST_WRITE_PERMISSION = 2;

    @BindView(R.id.drawing_view)
    DrawingView mDrawingView;

    @BindView(R.id.pick_brush_size_button)
    ImageView mPickBrushSizeButton;

    @BindView(R.id.pick_brush_color_button)
    ImageView mPickBrushColorButton;

    @BindView(R.id.reset_doodle_button)
    ImageView mResetDoodleButton;

    @BindView(R.id.start_new_doodle_button)
    Button mStartNewDoodleButton;

    @BindView(R.id.import_from_gallery_button)
    Button mImportFromGalleryButton;

    @BindView(R.id.initial_choice_buttons_layout)
    View mInitialChoiceButtonsLayout;

    @BindView(R.id.drawing_buttons_layout)
    View mDrawingButtonsLayout;

    @Inject
    ViewHelper mViewHelper;

    @Inject
    ColorPickerHelper mColorPickerHelper;

    @Inject
    DoodlePresenter mPresenter;

    private ProgressDialog mProgressDialog;

    private boolean mIsDrawingEnabled;

    private DoodleActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = (DoodleActivity) context;
        mActivity.getComponent().plusFragmentComponent().inject(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.startPresenting(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.stopPresenting();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.doodle_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPickBrushSizeButton.setOnClickListener(v -> showChooseBrushSizeDialog());
        mResetDoodleButton.setOnClickListener(v -> showClearScreenDialog());
        mImportFromGalleryButton.setOnClickListener(v -> selectImageFromGallery());

        mPickBrushColorButton.setOnClickListener(
                (View v) -> mColorPickerHelper.showPickColorDialog(R.string.choose_brush_color,
                        mDrawingView.getBrushColor(),
                        color -> mDrawingView.setBrushColor(color)));

        mStartNewDoodleButton.setOnClickListener(
                v -> mColorPickerHelper.showPickColorDialog(R.string.choose_background_color,
                        ContextCompat.getColor(mActivity, android.R.color.holo_green_dark),
                        color -> {
                            setDrawingMode(true);
                            mDrawingView.initWithColor(color);
                        }));
    }

    private void setDrawingMode(boolean isDrawingEnabled) {
        mIsDrawingEnabled = isDrawingEnabled;
        mDrawingView.setVisibility(isDrawingEnabled ? VISIBLE : View.INVISIBLE);
        mDrawingButtonsLayout.setVisibility(isDrawingEnabled ? VISIBLE : View.INVISIBLE);
        mInitialChoiceButtonsLayout.setVisibility(isDrawingEnabled ? View.INVISIBLE : VISIBLE);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (mIsDrawingEnabled) {
            inflater.inflate(R.menu.doodle_tabs, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_wallpaper:
                setDoodleAsWallpaper();
                return true;
            case R.id.action_save:
                trySavingToGallery();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == SELECT_PHOTO_RESULT && resultCode == RESULT_OK) {
            showImageFromGallery(intent.getData());
        }
    }

    private void showImageFromGallery(Uri uri) {
        showProgressDialog();
        mViewHelper.loadBitmapFromUri(uri, mDrawingView.getWidth(),
                mDrawingView.getHeight()).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                setDrawingMode(true);
                mDrawingView.initWithBitmap(resource);
                hideProgressDialog();
            }
        });
    }

    private void showChooseBrushSizeDialog() {
        ChooseBrushSizeDialogFragment chooseBrushSizeDialogFragment = new ChooseBrushSizeDialogFragment();
        chooseBrushSizeDialogFragment.setOnBrushSizeSelectedListener(brushSize -> mDrawingView.setBrushSize(brushSize));
        chooseBrushSizeDialogFragment.show(getChildFragmentManager(), null);
    }

    private void showClearScreenDialog() {
        AlertDialog.Builder clearScreenDialog = new AlertDialog.Builder(mActivity);
        clearScreenDialog.setTitle(R.string.clear_screen_dialog_title);
        clearScreenDialog.setMessage(R.string.clear_screen_dialog_body);
        clearScreenDialog.setPositiveButton(R.string.yes_button, (dialog, which) -> {
            setDrawingMode(false);
            dialog.dismiss();
        });
        clearScreenDialog.setNegativeButton(R.string.cancel_button, (dialog, which) -> dialog.cancel());
        clearScreenDialog.show();
    }


    private void setDoodleAsWallpaper() {
        showProgressDialog();
        mPresenter.setDoodleAsWallpaper(mDrawingView.getResultBitmap());
    }

    private void saveDoodleToGallery() {
        showProgressDialog();
        mPresenter.saveDoodleToGallery(mDrawingView.getResultBitmap());
    }

    private void showProgressDialog() {
        mProgressDialog = ProgressDialog.show(mActivity, getString(R.string.loading_title),
                getString(R.string.loading_body), true);
    }

    private void hideProgressDialog() {
        mProgressDialog.dismiss();
    }

    private void trySavingToGallery() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            saveDoodleToGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveDoodleToGallery();
            } else {
                Toast.makeText(mActivity, R.string.please_provide_local_storage_permission,
                        Toast.LENGTH_LONG).show();
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    navigateToAppSettings();
                }
            }
        }
    }

    public void selectImageFromGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO_RESULT);
    }

    public void navigateToAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void displaySetDoodleAsWallpaperSuccess() {
        hideProgressDialog();
        mViewHelper.displayToastMessage(R.string.set_doodle_as_wallpaper_success);
    }

    @Override
    public void displaySetDoodleAsWallpaperFailed() {
        hideProgressDialog();
        mViewHelper.displayErrorMessage(R.string.something_went_wrong_title,
                R.string.set_doodle_as_wallpaper_failed_body);
    }


    @Override
    public void displaySaveDoodleToGallerySuccess() {
        hideProgressDialog();
        mViewHelper.displayToastMessage(R.string.save_doodle_to_gallery_success);
    }

    @Override
    public void displaySaveDoodleToGalleryFailed() {
        hideProgressDialog();
        mViewHelper.displayErrorMessage(R.string.something_went_wrong_title,
                R.string.save_doodle_to_gallery_failed_body);
    }

}
