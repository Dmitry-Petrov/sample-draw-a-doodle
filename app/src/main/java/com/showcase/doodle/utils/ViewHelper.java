package com.showcase.doodle.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.showcase.doodle.R;

public class ViewHelper {

    private Activity mActivity;

    public ViewHelper(Activity activity) {
        mActivity = activity;
    }

    public void displayErrorMessage(int title, int message) {
        AlertDialog alertDialog = new AlertDialog.Builder(mActivity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(mActivity.getString(message));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, mActivity.getString(R.string.ok_button),
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    public void displayToastMessage(int message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    public BitmapRequestBuilder<Uri, Bitmap> loadBitmapFromUri(Uri uri, int targetWidth,
            int targetHeight) {
        return Glide.with(mActivity)
                .load(uri)
                .asBitmap()
                .centerCrop()
                .override(targetWidth, targetHeight);
    }
}
