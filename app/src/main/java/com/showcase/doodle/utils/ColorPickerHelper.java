package com.showcase.doodle.utils;

import android.app.Activity;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.showcase.doodle.R;

public class ColorPickerHelper {

    private Activity mActivity;

    public ColorPickerHelper(Activity activity) {
        mActivity = activity;
    }

    public void showPickColorDialog(int title, int initialColor, OnColorSelectedListener onColorSelectedListener) {
        ColorPickerDialogBuilder
                .with(mActivity)
                .setTitle(title)
                .initialColor(initialColor)
                .showAlphaSlider(false)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton(R.string.ok_button, (dialog, selectedColor, allColors) -> {
                    onColorSelectedListener.colorSelected(selectedColor);
                })
                .build()
                .show();
    }

    public interface OnColorSelectedListener {
        void colorSelected(int color);
    }
}
