package com.showcase.doodle.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.showcase.doodle.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseBrushSizeDialogFragment extends DialogFragment implements View.OnClickListener {

    public static final int SMALL_BRUSH_SIZE_DP = 10;
    public static final int MEDIUM_BRUSH_SIZE_DP = 20;
    public static final int LARGE_BRUSH_SIZE_DP = 30;

    @BindView(R.id.small_brush)
    View mSmallBrush;

    @BindView(R.id.medium_brush)
    View mMediumBrush;

    @BindView(R.id.large_brush)
    View mLargeBrush;

    private OnBrushSizeSelectedListener mOnBrushSizeSelectedListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ChooseBrushDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choose_brush_fragment, container);
        ButterKnife.bind(this, view);
        getDialog().setTitle(R.string.choose_brush_dialog_title);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSmallBrush.setOnClickListener(this);
        mMediumBrush.setOnClickListener(this);
        mLargeBrush.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.small_brush:
                mOnBrushSizeSelectedListener.brushSizeSelected(SMALL_BRUSH_SIZE_DP);
                break;
            case R.id.medium_brush:
                mOnBrushSizeSelectedListener.brushSizeSelected(MEDIUM_BRUSH_SIZE_DP);
                break;
            case R.id.large_brush:
                mOnBrushSizeSelectedListener.brushSizeSelected(LARGE_BRUSH_SIZE_DP);
                break;
        }
        dismiss();
    }

    public void setOnBrushSizeSelectedListener(OnBrushSizeSelectedListener onBrushSizeSelectedListener) {
        mOnBrushSizeSelectedListener = onBrushSizeSelectedListener;
    }

    public interface OnBrushSizeSelectedListener {
        void brushSizeSelected(int brushSize);
    }

}