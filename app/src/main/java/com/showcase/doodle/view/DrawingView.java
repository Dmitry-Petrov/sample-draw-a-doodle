package com.showcase.doodle.view;

import static com.showcase.doodle.view.ChooseBrushSizeDialogFragment.MEDIUM_BRUSH_SIZE_DP;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

public class DrawingView extends View {

    private Canvas mMainCanvas;
    private Bitmap mBaseBitmap;
    private Bitmap mBackgroundImageBitmap;

    private Path mUserDrawingChunk;
    private Paint mCurrentPaint;

    private float mBrushSize;
    private int mBackgroundColor;

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mUserDrawingChunk = new Path();
        mCurrentPaint = new Paint();
        mCurrentPaint.setColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        mCurrentPaint.setAntiAlias(true);
        mCurrentPaint.setStrokeWidth(mBrushSize);
        mCurrentPaint.setStyle(Paint.Style.STROKE);
        mCurrentPaint.setStrokeJoin(Paint.Join.ROUND);
        mCurrentPaint.setStrokeCap(Paint.Cap.ROUND);
        setBrushSize(MEDIUM_BRUSH_SIZE_DP);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        initCanvas(width, height);
    }

    private void initCanvas(int width, int height) {
        mBaseBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mMainCanvas = new Canvas(mBaseBitmap);
        if (mBackgroundImageBitmap != null) {
            mMainCanvas.drawBitmap(mBackgroundImageBitmap, new Matrix(), null);
        } else {
            mBaseBitmap.eraseColor(mBackgroundColor);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(mBaseBitmap, 0, 0, null);
        canvas.drawPath(mUserDrawingChunk, mCurrentPaint);
    }

    public void initWithBitmap(Bitmap bitmap) {
        mBackgroundImageBitmap = bitmap;
        initCanvas(getWidth(), getHeight());
        invalidate();
    }

    public void initWithColor(int backgroundColor) {
        mBackgroundImageBitmap = null;
        this.mBackgroundColor = backgroundColor;
        initCanvas(getWidth(), getHeight());
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float xPosition = event.getX();
        float yPosition = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                mUserDrawingChunk.lineTo(xPosition, yPosition);
                mMainCanvas.drawPath(mUserDrawingChunk, mCurrentPaint);
                mUserDrawingChunk.reset();
                break;
            case MotionEvent.ACTION_DOWN:
                mUserDrawingChunk.moveTo(xPosition, yPosition);
                break;
            case MotionEvent.ACTION_MOVE:
                mUserDrawingChunk.lineTo(xPosition, yPosition);
                break;
        }
        invalidate();
        return true;

    }

    public void setBrushColor(int newColor) {
        mCurrentPaint.setColor(newColor);
    }

    public void setBrushSize(float brushSizeDp) {
        mBrushSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                brushSizeDp, getResources().getDisplayMetrics());
        mCurrentPaint.setStrokeWidth(mBrushSize);
    }


    public Bitmap getResultBitmap() {
        return mBaseBitmap;
    }

    public int getBrushColor() {
        return mCurrentPaint.getColor();
    }
}
