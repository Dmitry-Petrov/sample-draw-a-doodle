package com.showcase.doodle;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.showcase.doodle.di.components.ActivityComponent;
import com.showcase.doodle.di.modules.ActivityModule;
import com.showcase.doodle.view.DrawingViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoodleActivity extends AppCompatActivity {

    @BindView(R.id.view_pager)
    DrawingViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mActivityComponent = DoodleApplication.getComponent().plus(new ActivityModule(this));
        mActivityComponent.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doodle_activity);
        ButterKnife.bind(this);

        mViewPager.setAdapter(new DoodlePagerAdapter(getSupportFragmentManager()));
        mViewPager.setPagingEnabled(false);

        mTabLayout.setupWithViewPager(mViewPager);
    }

    public class DoodlePagerAdapter extends FragmentPagerAdapter {

        DoodlePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new DoodleFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.my_doodle_tab_title);
                case 1:
                    return getString(R.string.friends_doodle_tab_title);
            }
            return null;
        }
    }


    public ActivityComponent getComponent() {
        return mActivityComponent;
    }
}
