# sample-draw-a-doodle  
A simple Doodle drawing application. Allows to:  
  
  1.  Draw a Doodle with your fingers on clean canvas or on image imported from gallery.  
  2.  Select background color, paint color and brush size.  
  3.  Export Doodle to Gallery.  
  4.  Set Doodle as a wallaper.  
  5.  Play with your friend on second canvas for an ultimate multiplayer expirience:)  
